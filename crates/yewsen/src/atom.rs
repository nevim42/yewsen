#![allow(dead_code)]
use std::{cell::RefCell, ops::Deref, rc::Rc};
use yew::prelude::*;

use crate::{listener::Listener, state::YewsenRoot};

pub trait Atom: PartialEq + Default + std::fmt::Debug {
    fn name() -> &'static str;
}

pub(crate) struct AtomState<T>
where
    T: Atom,
{
    pub(crate) inner: Rc<T>,
    callbacks: Rc<RefCell<Vec<Callback<Rc<T>>>>>, // Callbacks use in hooks
    listeners: Rc<RefCell<Vec<Listener<Rc<T>>>>>, // Listeners used outside hooks
}

impl<T> Default for AtomState<T>
where
    T: Atom,
{
    fn default() -> Self {
        Self {
            inner: Rc::new(T::default()),
            callbacks: Rc::new(RefCell::new(vec![])),
            listeners: Rc::new(RefCell::new(vec![])),
        }
    }
}

impl<T> From<T> for AtomState<T>
where
    T: Atom,
{
    fn from(value: T) -> Self {
        Self {
            inner: Rc::new(value),
            callbacks: Rc::new(RefCell::new(vec![])),
            listeners: Rc::new(RefCell::new(vec![])),
        }
    }
}

impl<T> Clone for AtomState<T>
where
    T: Atom,
{
    fn clone(&self) -> Self {
        Self {
            inner: self.inner.clone(),
            callbacks: self.callbacks.clone(),
            listeners: self.listeners.clone(),
        }
    }
}

impl<T> AtomState<T>
where
    T: Atom,
{
    pub fn add_callback(&self, cb: Callback<Rc<T>>) {
        self.callbacks.borrow_mut().push(cb);
    }

    pub fn remove_callback(&self, cb: Callback<Rc<T>>) {
        let found = self.callbacks.borrow().iter().position(|c| c.eq(&cb));
        if let Some(idx) = found {
            self.callbacks.borrow_mut().swap_remove(idx);
        }
    }

    pub fn add_listener(&self, lsn: Listener<Rc<T>>) {
        self.listeners.borrow_mut().push(lsn);
    }

    pub fn remove_listener(&self, lsn: Listener<Rc<T>>) {
        let found = self.listeners.borrow().iter().position(|l| l.eq(&lsn));
        if let Some(idx) = found {
            self.listeners.borrow_mut().swap_remove(idx);
        }
    }

    pub fn clone_with_new_value(&self, value: T) -> Self {
        Self {
            inner: Rc::new(value),
            callbacks: self.callbacks.clone(),
            listeners: self.listeners.clone(),
        }
    }

    pub fn notify(&self) {
        let cbs = self.callbacks.borrow().clone();
        let lsns = self.listeners.borrow().clone();

        for cb in cbs.iter() {
            cb.emit(self.inner.clone());
        }

        for lsn in lsns.iter() {
            lsn.emit(self.inner.clone());
        }
    }
}

pub struct UseAtomHandle<T>
where
    T: Atom + 'static,
{
    inner: Rc<T>,
    root: YewsenRoot,
}

impl<T> Deref for UseAtomHandle<T>
where
    T: Atom + 'static,
{
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &*self.inner
    }
}

impl<T> Clone for UseAtomHandle<T>
where
    T: Atom + 'static,
{
    fn clone(&self) -> Self {
        Self {
            inner: self.inner.clone(),
            root: self.root.clone(),
        }
    }
}

impl<T> UseAtomHandle<T>
where
    T: Atom + 'static,
{
    pub fn set(&self, value: T) {
        self.root.set_atom_state::<T>(value);
    }
}

#[hook]
pub fn use_atom<T>() -> UseAtomHandle<T>
where
    T: Atom + 'static,
{
    let root = use_context::<YewsenRoot>().expect(
        "Yewsen state was not initialized! Check if you add Yewsen provider into you application",
    );

    // "Copy" atom state value into Yew state
    let value = {
        let root = root.clone();
        use_state_eq(move || root.get_atom_state::<T>())
    };

    // Register callback to Atom and update state when Atom changes
    use_effect({
        let root = root.clone();
        let value = value.clone();
        move || {
            // Make sure we have current value in state
            value.set(root.get_atom_state::<T>());

            // Define callback that update state value when atom value changed
            let cb = Callback::from(move |new_value: Rc<T>| {
                value.set(new_value);
            });
            root.add_atom_callback::<T>(cb.clone());

            move || {
                // Called in unmount, we no need to listen for changes
                root.remove_atom_callback::<T>(cb);
            }
        }
    });

    UseAtomHandle {
        inner: (*value).clone(),
        root: root.clone(),
    }
}
