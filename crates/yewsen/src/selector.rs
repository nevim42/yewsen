#![allow(dead_code)]
use std::{cell::RefCell, ops::Deref, rc::Rc, fmt::Write};
use yew::prelude::*;

use crate::{
    listener::Listener,
    state::{YewsenRoot, YewsenSelectorHandle},
};

pub trait Selector: Default + PartialEq + Sized + std::fmt::Debug {
    type Input: 'static + PartialEq;

    fn select(yewsen: YewsenSelectorHandle<Self>, input: Rc<Self::Input>) -> Self;
}

pub(crate) struct SelectorState<S>
where
    S: Selector,
{
    pub(crate) inner: Rc<S>,
    pub(crate) input: Rc<S::Input>,
    callbacks: Rc<RefCell<Vec<Callback<Rc<S>>>>>, // Callbacks use in hooks
    listeners: Rc<RefCell<Vec<Listener<Rc<S>>>>>, // Listeners used outside hooks

    // Listeners called after selector update, to clean up listeners for example
    before_update: Rc<RefCell<Vec<Listener<()>>>>,
}

impl<S> std::fmt::Debug for SelectorState<S>
where
    S: Selector,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("SelectorState {{ {:?} }}", *self.inner))
    }
}

impl<S> Clone for SelectorState<S>
where
    S: Selector,
{
    fn clone(&self) -> Self {
        Self {
            inner: self.inner.clone(),
            input: self.input.clone(),
            callbacks: self.callbacks.clone(),
            listeners: self.listeners.clone(),
            before_update: self.before_update.clone(),
        }
    }
}

impl<S> SelectorState<S>
where
    S: Selector,
{
    pub fn add_callback(&self, cb: Callback<Rc<S>>) {
        self.callbacks.borrow_mut().push(cb);
    }

    pub fn remove_callback(&self, cb: Callback<Rc<S>>) {
        let found = self.callbacks.borrow().iter().position(|c| c.eq(&cb));
        if let Some(idx) = found {
            self.callbacks.borrow_mut().swap_remove(idx);
        }
    }

    pub fn add_listener(&self, lsn: Listener<Rc<S>>) {
        self.listeners.borrow_mut().push(lsn);
    }

    pub fn remove_listener(&self, lsn: Listener<Rc<S>>) {
        let found = self.listeners.borrow().iter().position(|l| l.eq(&lsn));
        if let Some(idx) = found {
            self.listeners.borrow_mut().swap_remove(idx);
        }
    }

    pub fn default_input(input: Rc<S::Input>) -> Self {
        Self {
            inner: Rc::new(S::default()),
            input: input.clone(),
            callbacks: Rc::new(RefCell::new(vec![])),
            listeners: Rc::new(RefCell::new(vec![])),
            before_update: Rc::new(RefCell::new(vec![])),
        }
    }

    pub fn clone_with_new_value(&self, value: S) -> Self {
        Self {
            inner: Rc::new(value),
            input: self.input.clone(),
            callbacks: self.callbacks.clone(),
            listeners: self.listeners.clone(),
            before_update: self.before_update.clone(),
        }
    }

    pub fn from_select(yewsen: YewsenSelectorHandle<S>, input: Rc<S::Input>) -> Self {
        let inner = S::select(yewsen, input.clone());

        Self {
            inner: Rc::new(inner),
            input,
            callbacks: Rc::new(RefCell::new(vec![])),
            listeners: Rc::new(RefCell::new(vec![])),
            before_update: Rc::new(RefCell::new(vec![])),
        }
    }

    pub fn from_value(value: S, input: Rc<S::Input>) -> Self {
        Self {
            inner: Rc::new(value),
            input,
            callbacks: Rc::new(RefCell::new(vec![])),
            listeners: Rc::new(RefCell::new(vec![])),
            before_update: Rc::new(RefCell::new(vec![])),
        }
    }

    pub fn notify(&self) {
        let cbs = self.callbacks.borrow().clone();
        let lsns = self.listeners.borrow().clone();

        for cb in cbs.iter() {
            cb.emit(self.inner.clone());
        }

        for lsn in lsns.iter() {
            lsn.emit(self.inner.clone());
        }
    }

    pub fn add_cleanup(&self, lsn: Listener<()>) {
        self.before_update.borrow_mut().push(lsn);
    }

    pub fn before_update(&self) {
        let lsns = self.before_update.borrow().clone();
        self.before_update.borrow_mut().clear();

        for lsn in lsns.iter() {
            lsn.emit(());
        }
    }
}

pub struct UseSelectorHandle<S>
where
    S: Selector + 'static,
{
    inner: Rc<S>,
    root: YewsenRoot,
}

impl<S> Deref for UseSelectorHandle<S>
where
    S: Selector + 'static,
{
    type Target = S;

    fn deref(&self) -> &Self::Target {
        &*self.inner
    }
}

#[hook]
pub fn use_selector<S>(input: Rc<S::Input>) -> UseSelectorHandle<S>
where
    S: Selector + 'static,
{
    let root = yew::use_context::<YewsenRoot>().expect(
        "Yewsen state was not initialized! Check if you add Yewsen provider into you application",
    );

    // "Copy" atom state value into Yew state
    let value = {
        let root = root.clone();
        let input = input.clone();
        use_state_eq(move || root.get_selector_state::<S>(input))
    };

    // Register callback to Atom and update state when Atom changes
    use_effect({
        let root = root.clone();
        let value = value.clone();
        let input = input.clone();
        move || {
            // Make sure we have current value in state
            value.set(root.get_selector_state::<S>(input.clone()));

            // Define callback that update state value when atom value changed
            let cb = Callback::from(move |new_value: Rc<S>| {
                value.set(new_value);
            });
            root.add_selector_callback::<S>(input.clone(), cb.clone());

            move || {
                // Called in unmount, we no need to listen for changes
                root.remove_selector_callback::<S>(input, cb);
            }
        }
    });

    UseSelectorHandle {
        inner: (*value).clone(),
        root: root.clone(),
    }
}
