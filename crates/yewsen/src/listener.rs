use std::rc::Rc;

pub struct Listener<IN>(Rc<dyn Fn(IN)>);

impl<IN, F: Fn(IN) + 'static> From<F> for Listener<IN> {
    fn from(func: F) -> Self {
        Self(Rc::new(func))
    }
}

impl<IN> Clone for Listener<IN> {
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}

impl<IN> PartialEq for Listener<IN> {
    fn eq(&self, other: &Listener<IN>) -> bool {
        Rc::ptr_eq(&self.0, &other.0)
    }
}

impl<IN> Listener<IN> {
    pub fn emit(&self, value: IN) {
        (*self.0)(value);
    }
}
