#![allow(dead_code)]
use anymap2::AnyMap;
use std::marker::PhantomData;
use std::{cell::RefCell, rc::Rc};
use yew::prelude::*;

use crate::atom::{Atom, AtomState};
use crate::listener::Listener;
use crate::selector::{Selector, SelectorState};

pub(crate) type StatesMap = AnyMap;
pub(crate) type SelectorsMap = AnyMap;

pub struct YewsenRoot {
    id: usize,
    states: Rc<RefCell<StatesMap>>,
    selectors: Rc<RefCell<SelectorsMap>>,
}

impl Default for YewsenRoot {
    fn default() -> Self {
        Self {
            id: 0, // For now, but we need some unique ID
            states: Rc::new(RefCell::new(StatesMap::new())),
            selectors: Rc::new(RefCell::new(StatesMap::new())),
        }
    }
}

impl Clone for YewsenRoot {
    fn clone(&self) -> Self {
        Self {
            id: self.id,
            states: self.states.clone(),
            selectors: self.selectors.clone(),
        }
    }
}

impl PartialEq for YewsenRoot {
    fn eq(&self, other: &Self) -> bool {
        other.id == self.id
    }
}

impl YewsenRoot {
    pub fn get_atom_state<T>(&self) -> Rc<T>
    where
        T: Atom + 'static,
    {
        // Happy pass (We already created AtomState in map)
        let states = self.states.borrow();
        if let Some(state) = states.get::<AtomState<T>>() {
            return state.inner.clone();
        }
        drop(states);

        // We need to create Default AtomState in map
        let state = AtomState::<T>::default();
        let mut states = self.states.borrow_mut();
        states.insert(state.clone());
        drop(states);

        state.inner.clone()
    }

    pub fn set_atom_state<T>(&self, value: T)
    where
        T: Atom + 'static,
    {
        let mut states = self.states.borrow_mut();
        let state = match states.remove::<AtomState<T>>() {
            Some(v) => v.clone_with_new_value(value),
            None => value.into(),
        };
        states.insert(state.clone());
        drop(states);

        // Now notify all listeners and callbacks
        state.notify();
    }

    pub fn add_atom_callback<T>(&self, cb: Callback<Rc<T>>)
    where
        T: Atom + 'static,
    {
        let states = self.states.borrow();
        if let Some(state) = states.get::<AtomState<T>>() {
            state.add_callback(cb);
        }
    }

    pub fn remove_atom_callback<T>(&self, cb: Callback<Rc<T>>)
    where
        T: Atom + 'static,
    {
        let states = self.states.borrow();
        if let Some(state) = states.get::<AtomState<T>>() {
            state.remove_callback(cb);
        }
    }

    pub fn add_atom_listener<T>(&self, lsn: Listener<Rc<T>>)
    where
        T: Atom + 'static,
    {
        let states = self.states.borrow();
        if let Some(state) = states.get::<AtomState<T>>() {
            state.add_listener(lsn);
        }
    }

    pub fn remove_atom_listener<T>(&self, lsn: Listener<Rc<T>>)
    where
        T: Atom + 'static,
    {
        let states = self.states.borrow();
        if let Some(state) = states.get::<AtomState<T>>() {
            state.remove_listener(lsn);
        }
    }

    pub fn get_selector_state<S>(&self, input: Rc<S::Input>) -> Rc<S>
    where
        S: Selector + 'static,
    {
        // Happy pass (We already created SelectorState in map)
        let selectors = self.selectors.borrow();
        if let Some(selector_vec) = selectors.get::<Vec<SelectorState<S>>>() {
            // Find selector with correct input value
            if let Some(selector) = selector_vec.iter().find(|s| (*s.input).eq(&*input)) {
                return selector.inner.clone();
            }
            drop(selectors);
        } else {
            drop(selectors);
               
            // There is no entry for selector states, create and empty array
            let mut selectors = self.selectors.borrow_mut();
            selectors.insert::<Vec<SelectorState<S>>>(vec![]);
            drop(selectors);
        }

        // Now we have prepared entry in map, so run the first select which will
        // add all listeners into our newly created entry
        let yewsen = self.get_selector_handle::<S>(input.clone());
        let value = S::select(yewsen, input.clone());

        let mut selectors = self.selectors.borrow_mut();
        let selector = SelectorState::<S>::from_value(value, input.clone());
        if let Some(selector_vec) = selectors.get_mut::<Vec<SelectorState<S>>>() {
            selector_vec.push(selector.clone());
        }
        drop(selectors);

        selector.inner.clone()
    }

    pub fn update_selector_state<S>(&self, input: Rc<S::Input>)
    where
        S: Selector + 'static,
    {
        if let Some(selector_vec) = self.selectors.borrow().get::<Vec<SelectorState<S>>>() {
            if let Some(selector) = selector_vec.iter().find(|s| (*s.input).eq(&*input)) {
                selector.before_update();
            }
        }

        // Check the array of selector states is created
        let mut selectors = self.selectors.borrow_mut();
        if let None = selectors.get::<Vec<SelectorState<S>>>() {
            selectors.insert::<Vec<SelectorState<S>>>(vec![]);
        }
        drop(selectors);

        let yewsen = self.get_selector_handle::<S>(input.clone());
        let value = S::select(yewsen, input.clone());
        
        let mut selectors = self.selectors.borrow_mut();
        let selector = if let Some(selector_vec) = selectors.get_mut::<Vec<SelectorState<S>>>() {
            let selector = if let Some(idx) = selector_vec.iter().position(|s| (*s.input).eq(&*input)) {
                let selector = selector_vec.get(idx).unwrap().clone_with_new_value(value);
                selector_vec.swap_remove(idx);
                selector
            } else {
                SelectorState::<S>::from_value(value, input.clone())
            };
            selector_vec.push(selector.clone());
            Some(selector)
        } else { None };
        drop(selectors);

        // Notify all listeners and callbacks about change
        if let Some(selector) = selector {
            selector.notify();
        }
    }

    pub fn add_selector_callback<S>(&self, input: Rc<S::Input>, cb: Callback<Rc<S>>)
    where
        S: Selector + 'static,
    {
        let selectors = self.selectors.borrow();
        if let Some(selector_vec) = selectors.get::<Vec<SelectorState<S>>>() {
            if let Some(selector) = selector_vec.iter().find(|s| (*s.input).eq(&*input)) {
                selector.add_callback(cb);
            }
        }
    }

    pub fn remove_selector_callback<S>(&self, input: Rc<S::Input>, cb: Callback<Rc<S>>)
    where
        S: Selector + 'static,
    {
        let selectors = self.selectors.borrow();
        if let Some(selector_vec) = selectors.get::<Vec<SelectorState<S>>>() {
            if let Some(selector) = selector_vec.iter().find(|s| (*s.input).eq(&*input)) {
                selector.remove_callback(cb);
            }
        }
    }

    pub fn add_selector_listener<S>(&self, input: Rc<S::Input>, lsn: Listener<Rc<S>>)
    where
        S: Selector + 'static,
    {
        let selectors = self.selectors.borrow();
        if let Some(selector_vec) = selectors.get::<Vec<SelectorState<S>>>() {
            if let Some(selector) = selector_vec.iter().find(|s| (*s.input).eq(&*input)) {
                selector.add_listener(lsn);
            }
        }
    }

    pub fn remove_selector_listener<S>(&self, input: Rc<S::Input>, lsn: Listener<Rc<S>>)
    where
        S: Selector + 'static,
    {
        let selectors = self.selectors.borrow();
        if let Some(selector_vec) = selectors.get::<Vec<SelectorState<S>>>() {
            if let Some(selector) = selector_vec.iter().find(|s| (*s.input).eq(&*input)) {
                selector.remove_listener(lsn);
            }
        }
    }

    pub fn add_selector_cleanup<S>(&self, input: Rc<S::Input>, lsn: Listener<()>)
    where
        S: Selector + 'static
    {
        let selectors = self.selectors.borrow();
        if let Some(selector_vec) = selectors.get::<Vec<SelectorState<S>>>() {
            if let Some(selector) = selector_vec.iter().find(|s| (*s.input).eq(&*input)) {
                selector.add_cleanup(lsn);
            }
        } else {
            log::warn!("This listener will never be cleanup");
        }
    }

    fn get_selector_handle<S>(&self, input: Rc<S::Input>) -> YewsenSelectorHandle<S>
    where
        S: Selector,
    {
        YewsenSelectorHandle::<S> {
            root: self.clone(),
            input,
            _marker: PhantomData,
        }
    }
}

#[derive(PartialEq)]
pub struct YewsenSelectorHandle<S>
where
    S: Selector,
{
    root: YewsenRoot,
    input: Rc<S::Input>,
    _marker: PhantomData<S>,
}

impl<S> YewsenSelectorHandle<S>
where
    S: Selector + 'static,
{
    pub fn get_atom_value<T>(&self) -> Rc<T>
    where
        T: Atom + 'static,
    {
        // Register listener for the given selector S in an atom T
        let value = self.root.get_atom_state::<T>();
        let listener = Listener::from({
            let value = value.clone();
            let root = self.root.clone();
            let input = self.input.clone();
            move |new_value: Rc<T>| {
                // Check for changes. If there is no change, no need to refresh
                // selector value, just skip
                if value.eq(&new_value) {
                    return;
                }

                // Invoke recalculation of selector?
                root.update_selector_state::<S>(input.clone());
            }
        });
        let cleanup = Listener::from({
            let root = self.root.clone();
            let listener = listener.clone();
            move |_: ()| {
                root.remove_atom_listener::<T>(listener.clone());
            }
        });

        self.root.add_atom_listener::<T>(listener);
        self.root.add_selector_cleanup::<S>(self.input.clone(), cleanup);

        value
    }

    pub fn get_selector_value<Z>(&self, input: Rc<Z::Input>) -> Rc<Z>
    where
        Z: Selector + 'static
    {
        let value = self.root.get_selector_state::<Z>(input.clone());
        let listener = Listener::from({
            let value = value.clone();
            let root = self.root.clone();
            let input = self.input.clone();
            move |new_value: Rc<Z>| {
                if value.eq(&new_value) {
                    return;
                }

                root.update_selector_state::<S>(input.clone());
            }
        });
        let cleanup = Listener::from({
            let root = self.root.clone();
            let listener = listener.clone();
            let input = input.clone();
            move |_: ()| {
                root.remove_selector_listener::<Z>(input.clone(), listener.clone());
            }
        });

        self.root.add_selector_listener::<Z>(input.clone(), listener);
        self.root.add_selector_cleanup::<S>(self.input.clone(), cleanup);

        value
    }
}

#[derive(PartialEq, Properties)]
pub struct YewsenProviderProps {
    #[prop_or_default]
    pub children: Children,
}

#[function_component(YewsenProvider)]
pub fn yewsen_provider(props: &YewsenProviderProps) -> Html {
    let root = use_state_eq(|| YewsenRoot::default());

    html! {
        <ContextProvider<YewsenRoot> context={(*root).clone()}>
            {props.children.clone()}
        </ContextProvider<YewsenRoot>>
    }
}
