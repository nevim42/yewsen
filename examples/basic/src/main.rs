use yew::prelude::*;
use yewsen::prelude::*;

#[derive(Default, PartialEq)]
struct Counter {
    pub inner: usize,
}

impl Atom for Counter {}

#[function_component(Application)]
fn application() -> Html {
    let counter = use_atom::<Counter>();

    let onclick = {
        let counter = counter.clone();
        Callback::from(move |_| {
            let val = Counter { inner: counter.inner + 1 };
            counter.set(val);
        })
    };

    html! {
        <div>
            <div class="ui statistic">
                <div class="value">
                    {&counter.inner}
                </div>
                <div class="label">
                    {"# of clicks"}
                </div>
            </div>
            <br />
            <button {onclick} class="ui button">{"Increment"}</button>
        </div>
    }
}

#[function_component(Root)]
fn root() -> Html {
    html! {
        <YewsenProvider>
            <Application />
        </YewsenProvider>
    }
}

fn main() {
    yew::Renderer::<Root>::new().render();
}
