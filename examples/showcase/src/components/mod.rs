use yew::prelude::*;

#[derive(PartialEq, Properties)]
pub struct StatisticsProps {
    pub size: Option<String>,
    pub color: Option<String>,
    pub value: u32,
    pub label: String,
}

#[function_component(Statistics)]
pub fn statistics(props: &StatisticsProps) -> Html {
    let color = if let Some(color) = &props.color {
        color.clone()
    } else {
        "".to_string()
    };

    let size = if let Some(size) = &props.size {
        size.clone()
    } else {
        "".to_string()
    };

    html! {
        <div class={classes!("ui", size, color, "statistic")}>
            <div class="value">
                {&props.value}
            </div>
            <div class="label">
                {&props.label}
            </div>
        </div>
    }
}
