use yew::prelude::*;
use yew_router::prelude::*;

use crate::screens::{Home, One, Two, Three, Four};

#[derive(Clone, Debug, Routable, PartialEq)]
pub enum Route {
    #[at("/")]
    Home,
    #[at("/one")]
    One,
    #[at("/two")]
    Two,
    #[at("/three")]
    Three,
    #[at("/four")]
    Four,
}

pub fn switch(route: Route) -> Html {
    match route {
        Route::Home => html! { <Home /> },
        Route::One => html! { <One /> },
        Route::Two => html! { <Two /> },
        Route::Three => html! { <Three /> },
        Route::Four => html! { <Four /> },
    }
}
