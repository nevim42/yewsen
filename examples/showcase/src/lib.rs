use yew::prelude::*;
use yew_hooks::prelude::*;
use yew_router::prelude::*;
use yewsen::prelude::*;

use routes::{switch, Route};

mod components;
mod routes;
mod screens;
mod state;

#[function_component(Application)]
fn application() -> Html {
    let renders = use_renders_count();

    html! {
        <div class="ys root">
            <div class="ys menu">
                <Link<Route> to={Route::Home} classes="ui icon labeled button">
                    <i class="home icon" />
                    {"Home"}
                </Link<Route>>
                <Link<Route> to={Route::One} classes="ui icon labeled button">
                    <i class="cloud icon" />
                    {"One"}
                </Link<Route>>
                <Link<Route> to={Route::Two} classes="ui icon labeled button">
                    <i class="play icon" />
                    {"Two"}
                </Link<Route>>
                <Link<Route> to={Route::Three} classes="ui icon labeled button">
                    <i class="chart pie icon" />
                    {"Three"}
                </Link<Route>>
                <Link<Route> to={Route::Four} classes="ui icon labeled button">
                    <i class="wifi icon" />
                    {"Four"}
                </Link<Route>>
            </div>
            <div class="ys rest">
                <div class="ys header">
                    <div class="ui image label">
                        <i class="sync alternate icon" />
                        {"Re-renders"}
                        <div class="detail">{renders}</div>
                    </div>
                </div>
                <div class="ys content">
                    <Switch<Route> render={switch} />
                </div>
            </div>
        </div>
    }
}

#[function_component(Root)]
pub fn root() -> Html {
    html! {
        <BrowserRouter>
            <YewsenProvider>
                <Application />
            </YewsenProvider>
        </BrowserRouter>
    }
}
