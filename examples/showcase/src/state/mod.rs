use std::rc::Rc;

use yewsen::prelude::*;

#[derive(Default, PartialEq, Debug)]
pub struct Foo {
    pub value: u32,
}

impl Atom for Foo {
    fn name() -> &'static str {
        "Foo"
    }
}

#[derive(Default, PartialEq, Debug)]
pub struct Bar {
    pub value: u32,
}

impl Atom for Bar {
    fn name() -> &'static str {
        "Bar"
    }
}

#[derive(Debug, Default, PartialEq)]
pub struct IsEven {
    pub inner: bool,
}

impl Selector for IsEven {
    type Input = ();

    fn select(yewsen: YewsenSelectorHandle<Self>, _input: Rc<Self::Input>) -> Self {
        let foo = yewsen.get_atom_value::<Foo>();

        Self {
            inner: foo.value % 2 == 0,
        }
    }
}

#[derive(Debug, Default, PartialEq)]
pub struct Sum {
    pub inner: u32,
}

impl Selector for Sum {
    type Input = ();

    fn select(yewsen: YewsenSelectorHandle<Self>, _input: Rc<Self::Input>) -> Self {
        let foo = yewsen.get_atom_value::<Foo>();
        let bar = yewsen.get_atom_value::<Bar>();

        Self {
            inner: foo.value + bar.value,
        }
    }
}

#[derive(Debug, Default, PartialEq)]
pub struct OffsetSum {
    pub inner: u32,
}

impl Selector for OffsetSum {
    type Input = u32;

    fn select(yewsen: YewsenSelectorHandle<Self>, input: Rc<Self::Input>) -> Self {
        let sum = yewsen.get_selector_value::<Sum>(Rc::new(()));

        Self {
            inner: sum.inner + 42,
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct SimpleFetch {
    pub loading: bool,
    pub data: Option<u32>,
    pub error: Option<String>,
}

impl Default for SimpleFetch {
    fn default() -> Self {
        Self {
            loading: true,
            data: None,
            error: None,
        }
    }
}

impl Atom for SimpleFetch {
    fn name() -> &'static str {
        "SimpleFetch"
    }
}
