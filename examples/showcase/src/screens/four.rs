use anyhow::Result;
use gloo_timers::future::sleep;
use wasm_bindgen_futures::spawn_local;
use yew::prelude::*;
use yew_hooks::{use_renders_count, use_effect_once};
use yewsen::prelude::*;
use js_sys::Math;

use crate::components::Statistics;
use crate::state::SimpleFetch;

fn rand_bool() -> bool {
    let num = (Math::random() * 100.0).round() as i32;
    num % 2 == 0
}

async fn fetch_data() -> Result<u32> {
    sleep(std::time::Duration::from_secs(2)).await;
    if rand_bool() {
        Ok(42)
    } else {
        anyhow::bail!("You was out of luck!")
    }
}

#[function_component(FetchOnMount)]
fn fetch_on_mount() -> Html {
    let renders = use_renders_count();
    let simple = use_atom::<SimpleFetch>();

    use_effect_once({
        let simple = simple.clone();
        move || {
            // Here call the async function?
            spawn_local(async move {
                if simple.data.is_some() { return; };

                simple.set(SimpleFetch { loading: true, data: None, error: None });
                let data = fetch_data().await;
                if let Ok(val) = data {
                    simple.set(SimpleFetch { loading: false, data: Some(val), error: None });
                } else {
                    simple.set(SimpleFetch { loading: false, data: None, error: Some("Failed to fetch data?".to_string()) });
                }
            });
            || {}
        }
    });

    if simple.loading {
        return html! {
            <div class="ui brown segment" style="min-width: 640px">
                <h2 class="ui header">{"Fetch on mount"}</h2>
                <div class="ui two statistics">
                    <Statistics
                        value={renders as u32}
                        label="Re-renders"
                    />
                    <div class="ui brown statistic">
                        <div class="value">
                            {"Loading"}
                        </div>
                        <div class="label">
                            {"Async Data"}
                        </div>
                    </div>
                </div>
            </div>
        };
    }

    if let Some(msg) = &simple.error {
        return html! {
            <div class="ui brown segment" style="min-width: 640px">
                <h2 class="ui header">{"Fetch on mount"}</h2>
                <div class="ui two statistics">
                    <Statistics
                        value={renders as u32}
                        label="Re-renders"
                    />
                    <div class="ui brown statistic">
                        <div class="value">
                            {"Error!"}
                        </div>
                        <div class="label">
                            {"Async Data"}
                        </div>
                    </div>
                </div>
                <div class="ui floating brown message">
                    <p>{msg}</p>
                </div>
            </div>
        };
    }

    html! {
        <div class="ui brown segment" style="min-width: 640px">
            <h2 class="ui header">{"Fetch on mount"}</h2>
            <div class="ui two statistics">
                <Statistics
                    value={renders as u32}
                    label="Re-renders"
                />
                <Statistics
                    value={simple.data.unwrap()}
                    label="Async Data"
                    color="brown"
                />
            </div>
        </div>
    }
}

#[function_component(Four)]
pub fn four() -> Html {
    let renders = use_renders_count();

    html! {
        <div>
            <FetchOnMount />
            <div class="ui brown segment" style="min-width: 480px">
                <h2 class="ui header">{"The Four screen"}</h2>
                <div class="ui one statistics">
                    <Statistics
                        value={renders as u32}
                        label="Re-renders"
                    />
                </div>
            </div>
        </div>
    }
}
