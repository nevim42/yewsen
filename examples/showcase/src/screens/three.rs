use std::rc::Rc;

use yew::prelude::*;
use yew_hooks::use_renders_count;
use yewsen::prelude::*;

use crate::components::Statistics;
use crate::state::{Foo, Bar, IsEven, Sum, OffsetSum};

#[function_component(AtomControl)]
fn atom_control() -> Html {
    let renders = use_renders_count();
    let foo = use_atom::<Foo>();
    let bar = use_atom::<Bar>();

    let inc_foo = {
        let foo = foo.clone();
        Callback::from(move |_: MouseEvent| {
            let inc = Foo {
                value: foo.value + 1,
            };
            foo.set(inc);
        })
    };
    let dec_foo = {
        let foo = foo.clone();
        Callback::from(move |_: MouseEvent| {
            if foo.value == 0 { return ; }
            let dec = Foo {
                value: foo.value - 1,
            };
            foo.set(dec);
        })
    };

    let inc_bar = {
        let bar = bar.clone();
        Callback::from(move |_: MouseEvent| {
            let inc = Bar {
                value: bar.value + 1,
            };
            bar.set(inc);
        })
    };
    let dec_bar = {
        let bar = bar.clone();
        Callback::from(move |_: MouseEvent| {
            if bar.value == 0 { return ; }
            let dec = Bar {
                value: bar.value - 1,
            };
            bar.set(dec);
        })
    };

    html! {
        <div class="ui pink segment" style="min-width: 480px">
            <h2 class="ui header">{"Atom Control"}</h2>
            <div class="ui three statistics">
                <Statistics
                    value={renders as u32}
                    label="Re-renders"
                />
                <Statistics
                    value={foo.value}
                    label="Foo"
                />
                <Statistics
                    value={bar.value}
                    label="Bar"
                />
            </div>
            <div class="ui divider hidden" />
            <div class="ui horizontal divider">
                {"Foo control"}
            </div>
            <div class="ui buttons" style="width: 100%">
                <button onclick={inc_foo} class="ui icon labeled button">
                    <i class="plus icon" />
                    {"Inc foo"}
                </button>
                <button onclick={dec_foo} class="ui icon labeled button">
                    <i class="minus icon" />
                    {"Dec foo"}
                </button>
            </div>
            <div class="ui horizontal divider">
                {"Bar control"}
            </div>
            <div class="ui buttons" style="width: 100%">
                <button onclick={inc_bar} class="ui icon labeled button">
                    <i class="plus icon" />
                    {"Inc bar"}
                </button>
                <button onclick={dec_bar} class="ui icon labeled button">
                    <i class="minus icon" />
                    {"Dec bar"}
                </button>
            </div>
        </div>
    }
}

#[function_component(EvenSelector)]
fn even_selector() -> Html {
    let renders = use_renders_count();
    let even = use_selector::<IsEven>(Rc::new(()));

    html! {
        <div class="ui pink segment" style="min-width: 480px">
            <h2 class="ui header">{"Even Selector"}</h2>
            <div class="ui two statistics">
                <Statistics
                    value={renders as u32}
                    label="Re-renders"
                />
                <div class="ui pink statistic">
                    <div class="value">
                        {if even.inner == true { "Yes" } else { "No" }}
                    </div>
                    <div class="label">
                        {"Is Foo Even?"}
                    </div>
                </div>
            </div>
        </div>
    }
}

#[function_component(SumSelector)]
fn sum_selector() -> Html {
    let renders = use_renders_count();
    let sum = use_selector::<Sum>(Rc::new(()));

    html! {
        <div class="ui pink segment" style="min-width: 480px">
            <h2 class="ui header">{"Sum Selector"}</h2>
            <div class="ui two statistics">
                <Statistics
                    value={renders as u32}
                    label="Re-renders"
                />
                <Statistics
                    value={sum.inner}
                    label="Foo + Bar"
                    color="violet"
                />
            </div>
        </div>
    }
}

#[function_component(BothSelectors)]
fn both_selectors() -> Html {
    let renders = use_renders_count();
    let even = use_selector::<IsEven>(Rc::new(()));
    let sum = use_selector::<Sum>(Rc::new(()));

    html! {
        <div class="ui pink segment" style="min-width: 480px">
            <h2 class="ui header">{"Both Selectors"}</h2>
            <div class="ui three statistics">
                <Statistics
                    value={renders as u32}
                    label="Re-renders"
                />
                <div class="ui pink statistic">
                    <div class="value">
                        {if even.inner == true { "Yes" } else { "No" }}
                    </div>
                    <div class="label">
                        {"Is Foo Even?"}
                    </div>
                </div>
                <Statistics
                    value={sum.inner}
                    label="Foo + Bar"
                    color="violet"
                />
            </div>
        </div>
    }
}

#[function_component(SelectorFromSelector)]
fn selector_from_selector() -> Html {
    let renders = use_renders_count();
    let offset = use_selector::<OffsetSum>(Rc::new(42));

    html! {
        <div class="ui pink segment" style="min-width: 480px">
            <h2 class="ui header">{"Selector From Selector"}</h2>
            <div class="ui two statistics">
                <Statistics
                    value={renders as u32}
                    label="Re-renders"
                />
                <Statistics
                    value={offset.inner}
                    label="Foo + Bar + 42"
                    color="violet"
                />
            </div>
        </div>
    }
}



#[function_component(Three)]
pub fn three() -> Html {
    let renders = use_renders_count();

    html! {
        <div class="ui grid">
            <div class="eight wide column">
                <AtomControl />
                <div class="ui pink segment" style="min-width: 480px">
                    <h2 class="ui header">{"Pink Screen"}</h2>
                    <div class="ui one statistics">
                        <Statistics
                            value={renders as u32}
                            label="Re-renders"
                        />
                    </div>
                </div>
            </div>
            <div class="eight wide column">
                <EvenSelector />
                <SumSelector />
                <BothSelectors />
                <SelectorFromSelector />
            </div>
        </div>
    }
}
