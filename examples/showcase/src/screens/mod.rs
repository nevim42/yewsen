use yew::prelude::*;

pub use one::One;
pub use two::Two;
pub use three::Three;
pub use four::Four;

pub mod one;
pub mod two;
pub mod three;
pub mod four;

#[function_component(Home)]
pub fn home() -> Html {
    html! {
        <div class="ui violet segment" style="min-width: 480px">
            {"Home screen"}
        </div>
    }
}
