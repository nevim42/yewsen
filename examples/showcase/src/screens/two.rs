use yew::prelude::*;
use yew_hooks::use_renders_count;
use yewsen::prelude::*;

use crate::components::Statistics;
use crate::state::{Bar, Foo};

#[function_component(Counter)]
pub fn counter() -> Html {
    let renders = use_renders_count();

    let foo = use_atom::<Foo>();
    let bar = use_atom::<Bar>();

    let inc_foo = {
        let foo = foo.clone();
        Callback::from(move |_: MouseEvent| {
            let inc = Foo {
                value: foo.value + 1,
            };
            foo.set(inc);
        })
    };
    let dec_foo = {
        let foo = foo.clone();
        Callback::from(move |_: MouseEvent| {
            let dec = Foo {
                value: foo.value - 1,
            };
            foo.set(dec);
        })
    };

    let inc_bar = {
        let bar = bar.clone();
        Callback::from(move |_: MouseEvent| {
            let inc = Bar {
                value: bar.value + 1,
            };
            bar.set(inc);
        })
    };
    let dec_bar = {
        let bar = bar.clone();
        Callback::from(move |_: MouseEvent| {
            let dec = Bar {
                value: bar.value - 1,
            };
            bar.set(dec);
        })
    };

    html! {
        <div class="ui blue segment" style="min-width: 480px">
            <h2 class="ui header">{"Counter of Both"}</h2>
            <div class="ui three statistics">
                <Statistics
                    value={renders as u32}
                    label="Re-renders"
                />
                <Statistics
                    value={foo.value}
                    label="Foo"
                />
                <Statistics
                    value={bar.value}
                    label="Bar"
                />
            </div>
            <div class="ui divider hidden" />
            <div class="ui buttons">
                <button onclick={inc_foo} class="ui icon labeled button">
                    <i class="plus icon" />
                    {"Inc foo"}
                </button>
                <button onclick={dec_foo} class="ui icon labeled button">
                    <i class="minus icon" />
                    {"Dec foo"}
                </button>
                <button onclick={inc_bar} class="ui icon labeled button">
                    <i class="plus icon" />
                    {"Inc bar"}
                </button>
                <button onclick={dec_bar} class="ui icon labeled button">
                    <i class="minus icon" />
                    {"Dec bar"}
                </button>
            </div>
        </div>
    }
}

#[function_component(FooCounter)]
pub fn foo_counter() -> Html {
    let renders = use_renders_count();
    let foo = use_atom::<Foo>();

    let inc_foo = {
        let foo = foo.clone();
        Callback::from(move |_: MouseEvent| {
            let inc = Foo {
                value: foo.value + 1,
            };
            foo.set(inc);
        })
    };
    let dec_foo = {
        let foo = foo.clone();
        Callback::from(move |_: MouseEvent| {
            let dec = Foo {
                value: foo.value - 1,
            };
            foo.set(dec);
        })
    };

    html! {
        <div class="ui blue segment" style="min-width: 480px">
            <h2 class="ui header">{"Foo Counter"}</h2>
            <div class="ui two statistics">
                <Statistics
                    value={renders as u32}
                    label="Re-renders"
                />
                <Statistics
                    value={foo.value}
                    label="Foo"
                />
            </div>
            <div class="ui divider hidden" />
            <div class="ui buttons">
                <button onclick={inc_foo} class="ui icon labeled button">
                    <i class="plus icon" />
                    {"Inc foo"}
                </button>
                <button onclick={dec_foo} class="ui icon labeled button">
                    <i class="minus icon" />
                    {"Dec foo"}
                </button>
            </div>
        </div>
    }
}

#[function_component(BarCounter)]
pub fn bar_counter() -> Html {
    let renders = use_renders_count();
    let bar = use_atom::<Bar>();

    let inc_bar = {
        let bar = bar.clone();
        Callback::from(move |_: MouseEvent| {
            let inc = Bar {
                value: bar.value + 1,
            };
            bar.set(inc);
        })
    };
    let dec_bar = {
        let bar = bar.clone();
        Callback::from(move |_: MouseEvent| {
            let dec = Bar {
                value: bar.value - 1,
            };
            bar.set(dec);
        })
    };

    html! {
        <div class="ui blue segment" style="min-width: 480px">
            <h2 class="ui header">{"Bar Counter"}</h2>
            <div class="ui two statistics">
                <Statistics
                    value={renders as u32}
                    label="Re-renders"
                />
                <Statistics
                    value={bar.value}
                    label="Bar"
                />
            </div>
            <div class="ui divider hidden" />
            <div class="ui buttons">
                <button onclick={inc_bar} class="ui icon labeled button">
                    <i class="plus icon" />
                    {"Inc bar"}
                </button>
                <button onclick={dec_bar} class="ui icon labeled button">
                    <i class="minus icon" />
                    {"Dec bar"}
                </button>
            </div>
        </div>
    }
}

#[function_component(Two)]
pub fn two() -> Html {
    let renders = use_renders_count();

    html! {
        <div>
            <Counter />
            <FooCounter />
            <BarCounter />
            <div class="ui blue segment" style="min-width: 480px">
                <h2 class="ui header">{"Blue screen"}</h2>
                <div class="ui one statistics">
                    <Statistics
                        value={renders as u32}
                        label="Re-renders"
                    />
                </div>
            </div>
        </div>
    }
}
