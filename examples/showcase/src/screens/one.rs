use yew::prelude::*;
use yew_hooks::use_renders_count;
use yewsen::prelude::*;

use crate::components::Statistics;
use crate::state::{Bar, Foo};

#[function_component(Setter)]
pub fn setter() -> Html {
    let renders = use_renders_count();

    let foo = use_atom::<Foo>();
    let bar = use_atom::<Bar>();

    let set_foo = {
        let foo = foo.clone();
        Callback::from(move |_: MouseEvent| {
            let inc = Foo { value: 420 };
            foo.set(inc);
        })
    };
    let set_bar = {
        let bar = bar.clone();
        Callback::from(move |_: MouseEvent| {
            let inc = Bar { value: 69 };
            bar.set(inc);
        })
    };

    html! {
        <div class="ui olive segment" style="min-width: 480px">
            <h2 class="ui header">{"Setter of Foo and Bar"}</h2>
            <div class="ui three statistics">
                <Statistics
                    value={renders as u32}
                    label="Re-renders"
                />
                <Statistics
                    value={foo.value}
                    label="Foo"
                />
                <Statistics
                    value={bar.value}
                    label="Bar"
                />
            </div>
            <div class="ui divider hidden" />
            <div class="ui buttons">
                <button onclick={set_foo} class="ui icon labeled button">
                    <i class="hand point right icon" />
                    {"Set foo to 420"}
                </button>
                <button onclick={set_bar} class="ui icon labeled button">
                    <i class="hand point right icon" />
                    {"Set bar to 69"}
                </button>
            </div>
        </div>
    }
}

#[function_component(One)]
pub fn one() -> Html {
    let renders = use_renders_count();

    html! {
        <div>
            <Setter />
            <div class="ui olive segment" style="min-width: 480px">
                <h2 class="ui header">{"One screen"}</h2>
                <div class="ui one statistics">
                    <Statistics
                        value={renders as u32}
                        label="Re-renders"
                    />
                </div>
            </div>
        </div>
    }
}
